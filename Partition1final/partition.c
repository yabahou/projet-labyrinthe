#include <stdlib.h>
#include <stdio.h>
#include "partition.h"

/* ----------------------------------------------------------------------------------------------
   CETTE FONCTION CREER UNE PARTITION DONT CHAQUE ELEMENT EST UNE CLASSE DE L'ENSEMBLE E  */

Partition_t creation_partition(int longueur)
{
    Partition_t partition;
    partition.taille=longueur;                                // La taille
    partition.tableau=malloc(longueur*sizeof(int));           // allocation du tableau
    for (int i=0;i<partition.taille;i++)                      // Identification de chaque classe 
        partition.tableau[i]=i;     
    return partition;
}

void Affichage_partition(Partition_t partition)
{
    printf("Les elements :");
    for (int i=0; i<partition.taille;i++)
        printf("%d ",i);
    printf("\n");    
    printf("Leur classes :");    
    for (int i=0; i<partition.taille;i++)
        printf("%d ",partition.tableau[i]);
    printf("\n -----------------------");
    printf("\n");    
}

/* ----------------------------------------------------------------------------------------------
   CETTE FONCTION RETOURNE LA CLASSE D'UN ELEMENT DE NOTRE ENSEMBLE   */

int recherche_classe(Partition_t partition,int element) 
{  
    return partition.tableau[element];
}

/* ----------------------------------------------------------------------------------------------
   CETTE FONCTION FAIT LA FUSION DE DEUX ELEMENT DE LA PARTITION , DONC FUSIONNER LEUR CLASSE   */

Partition_t fusion(int x,int y,Partition_t partition)
{
        int cx=recherche_classe(partition,x);   // RECHERCHE DE LA CLASSE DE L ELEMENT
        int cy=recherche_classe(partition,y);   
        for (int i=0;i<partition.taille;i++)
        {
            if (partition.tableau[i]==cy)       // TOUT LES ELEMENT DE LA CLASSE cy SERONT DES ELEMENTS DE LA CLASSE cx
                partition.tableau[i]=cx;
        }
        return partition;          
}
/* ----------------------------------------------------------------------------------------------
   CETTE FONCTION AFFICHE LE CONTENU DU TABLEAU, DONC LES C   */
void lister_partition(Partition_t partition)
{
    for (int i=0;i<partition.taille;i++)
    {
        printf("%d",partition.tableau[i]);
    }
}


int * lister_classe(Partition_t partition, int classe)
{
    int * liste_classe=malloc(sizeof(int));
    int k=0;
    for (int i=0;i<partition.taille;i++)
        {
            if (partition.tableau[i]==classe)
               {  
                liste_classe[k]=i;
                k++; 
                liste_classe=(int*)realloc(liste_classe,k*sizeof(int));
               }      
        }
    return liste_classe;
}

void afficher_tableau(int *tableau)
{
    int i=0;
    while(tableau[i]!=NULL)
    {
        printf("%d  ",tableau[i]);
        i++;
    }
}

// lister_partition(Partition_t partition)
//     renvoie la liste des classes.

int main()
{
    int *liste_classe;
    Partition_t partition = creation_partition(10);
    Affichage_partition(partition);
    fusion(2,3,partition);
    Affichage_partition(partition);
    fusion(2,4,partition);
    Affichage_partition(partition);
    fusion(7,1,partition);
    Affichage_partition(partition);
    liste_classe=lister_classe(partition, 7);
    afficher_tableau(liste_classe);
 }

