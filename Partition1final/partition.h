#include <stdlib.h>
#include <stdio.h>


typedef struct Partition {
    int* tableau; // le debut du tableau
    int taille; // la taille du tableau
}Partition_t;   


Partition_t creation_partition(int longueur);
void Affichage_partition(Partition_t partition);
int recherche_classe(Partition_t partition,int element);
Partition_t fusion(int x,int y,Partition_t partition);
void lister_partition(Partition_t partition);
int * lister_classe(Partition_t partition, int classe);
void afficher_tableau(int *tableau);


