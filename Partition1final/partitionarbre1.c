#include <stdlib.h>
#include <stdio.h>
#include "partitionarbre1.h"

/* ----------------------------------------------------------------------------------------------

    Trouver la classe d’un élément se fait en temps constant, mais fusionner deux classes prend
    un temps O(n), puisqu’il faut parcourir tout le tableau pour repérer les éléments dont il faut
    changer la classe. Une deuxième solution, que nous détaillons maintenant, consiste à choisir un
    représentant dans chaque classe. Fusionner deux classes revient alors à changer de représentant
    pour les éléments de la classe fusionnée. Il apparaı̂t avantageux de représenter la partition par
    une forêt. Chaque classe de la partition constitue un arbre de cette forêt. La racine de l’arbre
    est le représentant de sa classe.

 */

/* ----------------------------------------------------------------------------------------------
   DANS L IMPLEMENTATION ARBROESCENTE , ON UTILISE LA HAUTEUR QUI SERA PRINCIPALEMENT UTILISE DANS LA FUSION  */

partition_t initialisation(int taille)
{       
        partition_t partition;
        partition.taille=taille;
        partition.pere=malloc(taille*sizeof(int));
        partition.hauteur=malloc(taille*sizeof(int));
        for (int i = 0; i < partition.taille ; i++)
             {
                  partition.pere[i] = i;
                  partition.hauteur[i]=1;                   // LA HAUTEUR EST 1 INITIALEMENT
             }
        return partition ;    
}

/* ----------------------------------------------------------------------------------------------
   AFFICHAGE DES ELEMENTS DE L ENSEMBLE AVEC LEUR PERE ASSOCIE  */

void Affichage_partition(partition_t partition)
{
    printf("Les elements :");
    for (int i=0; i<partition.taille;i++)
        printf("%d ",i);
    printf("\n");    
    printf("Leur peres   :");    
    for (int i=0; i<partition.taille;i++)
        printf("%d ",partition.pere[i]);
    printf("\n -----------------------");
    printf("\n");    
}

/* ----------------------------------------------------------------------------------------------
   Chercher le représentant de la classe contenant un élément donné X revient à trouver la racine de
   l’arbre contenant un noeud donné. Ceci se fait par la méthode suivante :   */

int trouver(partition_t partition,int x)

{
    while (x != partition.pere[x])
        x = partition.pere[x];
    return x;
}

/* ----------------------------------------------------------------------------------------------
    Chacune des deux méthodes est de complexité O(h), où h
    est la hauteur l’arbre (la plus grande des hauteurs des deux arbres). En fait, on peut améliorer
    l’efficacité de l’algorithme par la règle suivante 
    
    
    Lors de l’union de deux arbres, la racine de l’arbre de moindre taille devient fils de la
    racine de l’arbre de plus grande taille.  */


partition_t fusion(partition_t partition,int x, int y)
{
    int r = trouver(partition,x);                           // on cherche la racine de x et de y
    int s = trouver(partition,y);
    if (partition.hauteur[r] >= partition.hauteur[s])       
    {
        partition.pere[s] = r;
        if(partition.hauteur[r] == partition.hauteur[s])
             partition.hauteur[r]+=partition.hauteur[s];
    }
    else if (partition.hauteur[r] < partition.hauteur[s])
    {
        partition.pere[r] = s;
        partition.hauteur[s]+=partition.hauteur[r];
    }
    return partition;
}


int * lister_composantes(partition_t partition, int pere)
{
    int * liste_composantes=malloc(sizeof(int));
    //int *tableau=malloc(partition.taille*sizeof(int));
    int k=1;
    for (int i=0;i<partition.taille;i++)
        {
            if (partition.pere[i]==pere)
               {  
                liste_composantes[k]=i;
                k++; 
                liste_composantes=(int*)realloc(liste_composantes,k*sizeof(int));
               }      
        }
    liste_composantes[0]=k;
    return liste_composantes;
}

int existe(int * liste,int taille,int element)
{
    int code=0;
    for (int i=0;i<taille;i++)
    {
        if (liste[i]==element)
             code=1;
    }
    return code;
}

int nombre_connexe(partition_t  partition)
{
    int nombrepere;
    int * listepere=malloc(partition.taille*sizeof(int));
    for(int i=0;i<partition.taille;i++)
    {
        if  (!existe(listepere,i,partition.pere[i]))
            nombrepere++;
        listepere[i]=partition.pere[i];  
    }
    free(listepere);
    return nombrepere;
}

// int * listepere(partition_t partition)
// {
//     int nombre=nombre_connexe(partition);

// }

 void affichage_composante(int * liste)
{
     printf("la composante connexe contient les noeuds :");
     for(int i=1;i<liste[0];i++)
     {
        printf(" %d ",liste[i]);
     }
}



// int ** composantes(partition_t partition)
// {
//     lister_composantes(partition_t partition, int pere)
//     int nombreconnexe=nombre_connexe(partition);
//     int * pere=malloc(nombreconnexe*sizeof(int));
//     affichage_composante(pere);
// }

// --------------------------------------------------------------------------
// int ** liste_connexe(partition_t partition)
// {
//     int nombreconnexe=nombre_connexe(partition);
//     int ** liste=malloc(nombreconnexe*sizeof(int));
//     for (int i=0;i<nombreconnexe;i++)
//     {
//         lister_composantes(partition, )
//         liste[i]=malloc()
//     }
// }


int main ()
{
    partition_t partition;
    partition=initialisation(11);
    Affichage_partition(partition);
    fusion(partition,0,1);
    Affichage_partition(partition);
    fusion(partition,2,3);
    Affichage_partition(partition);
    fusion(partition,10,3);
    Affichage_partition(partition);
    fusion(partition,5,9);
    Affichage_partition(partition);
    fusion(partition,4,6);
    Affichage_partition(partition);
    fusion(partition,8,7); 
    Affichage_partition(partition);
    fusion(partition,9,7);
    Affichage_partition(partition);
    fusion(partition,6,8);
    Affichage_partition(partition);
 }


