#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include "laby.h"
#include <math.h>
#include <stdio.h>
#include <SDL2/SDL.h>
#include <string.h>
#include <SDL2/SDL_image.h> 
#include <stdbool.h>
#define N 1
#define E 2
#define S 4
#define O 8
//gcc  *.c -o Labyrinth -lSDL2 -lSDL2_gfx -lSDL2_image -lSDL2_ttf -lSDL2_mixer -lSDL2_net -lm 


/* 

La fonction loadImage doit charger une image et renvoyer la texture correspondante. Elle doit donc charger l’image dans une surface, 
convertir cette surface en texture et renvoyer cette texture sans oublier de vérifier les retours des fonctions employées. 

    1- on crée une surface (type de données SDL1)
    2- on charge l'image dans la SDL_Surface,
    3- on transforme la surface en texture (type de données correspondant en SDL2)
   
*/

void end_sdl(char ok,                                                 // fin normale : ok = 0 ; anormale ok = 1
                  char const* msg,                                    // message à afficher
                  SDL_Window* window,                                 // fenêtre à fermer
                  SDL_Renderer* renderer) {                           // renderer à fermer
  char msg_formated[255];                                         
  int l;                                                          

  if (!ok) {                                                      
         strncpy(msg_formated, msg, 250);                                 
         l = strlen(msg_formated);                                        
         strcpy(msg_formated + l, " : %s\n");                     

         SDL_Log(msg_formated, SDL_GetError());                   
  }                                                               

  if (renderer != NULL) SDL_DestroyRenderer(renderer);                            
  if (window != NULL)   SDL_DestroyWindow(window);                                        

  SDL_Quit();                                                     

  if (!ok) {                                                      
         exit(EXIT_FAILURE);                                              
  }                                                               
}        

void tracer_graphe(graphe_t graphe, char* fichier)
{
    FILE* fic = NULL;
    fic = fopen(fichier, "w");
    if (fic != NULL)
    {
        fprintf(fic, "graph G {\n");
        for (int k=0; k<graphe.nombrearrete;k++)
        {
            fprintf(fic,"%d--%d;\n",graphe.arrete[k].i,graphe.arrete[k].j);
        }
        fprintf(fic, "}\n");
    }
}          
SDL_Texture* load_texture_from_image(char  *  file_image_name, SDL_Window *window, SDL_Renderer *renderer )
{
    SDL_Surface *my_image = NULL;           // Variable de passage
    SDL_Texture* my_texture = NULL;         // La texture

    my_image = IMG_Load(file_image_name);   // Chargement de l'image dans la surface
                                            // image=SDL_LoadBMP(file_image_name); fonction standard de la SDL, 
                                            // uniquement possible si l'image est au format bmp */
    if (my_image == NULL) end_sdl(0, "Chargement de l'image impossible", window, renderer);

    my_texture = SDL_CreateTextureFromSurface(renderer, my_image); // Chargement de l'image de la surface vers la texture
    SDL_FreeSurface(my_image);                                     // la SDL_Surface ne sert que comme élément transitoire 
    if (my_texture == NULL) end_sdl(0, "Echec de la transformation de la surface en texture", window, renderer);

    return my_texture;
}


void play_with_texture_1(SDL_Texture *texture_back, SDL_Window *window,
                         SDL_Renderer *renderer) {
  SDL_Rect 
          source = {0},                         // Rectangle définissant la zone de la texture à récupérer
          window_dimensions = {0},              // Rectangle définissant la fenêtre, on n'utilisera que largeur et hauteur
          destination = {0};                    // Rectangle définissant où la zone_source doit être déposée dans le renderer

  SDL_GetWindowSize(
      window, &window_dimensions.w,
      &window_dimensions.h);                    // Récupération des dimensions de la fenêtre
  SDL_QueryTexture(texture_back, NULL, NULL,
                   &source.w, &source.h);       // Récupération des dimensions de l'image

  destination = window_dimensions;              // On fixe les dimensions de l'affichage à  celles de la fenêtre

  /* On veut afficher la texture de façon à ce que l'image occupe la totalité de la fenêtre */

  SDL_RenderCopy(renderer, texture_back,
                 &source,
                 &destination);
               // Création de l'élément à afficher
                   // Affichage

}
void play_with_texture_1_1(SDL_Texture *my_texture, SDL_Window *window,
                         SDL_Renderer *renderer) 
{
  SDL_Rect 
          source_image = {0},                         // Rectangle définissant la zone de la texture à récupérer
          window_dimensions = {0},              // Rectangle définissant la fenêtre, on n'utilisera que largeur et hauteur
          destination = {0};                    // Rectangle définissant où la zone_source_image doit être déposée dans le renderer

  SDL_GetWindowSize(
      window, &window_dimensions.w,
      &window_dimensions.h);                    // Récupération des dimensions de la fenêtre
  SDL_QueryTexture(my_texture, NULL, NULL,
                   &source_image.w, &source_image.h);       // Récupération des dimensions de l'image
  destination = window_dimensions;              // On fixe les dimensions de l'affichage à  celles de la fenêtre
  /* On veut afficher la texture de façon à ce que l'image occupe la totalité de la fenêtre */
  SDL_RenderCopy(renderer, my_texture,
                 &source_image,
                 &destination);                 // Création de l'élément à afficher
  SDL_RenderPresent(renderer);                  // Affichage
}

  
// Affichage d'une texture sur la totalité de la fenêtre
void play_with_texture_2(SDL_Texture *my_texture,SDL_Texture *texture_back, SDL_Window *window,
                         SDL_Renderer *renderer,int** matrice,int taille) 
{
  SDL_Rect 
          source_image = {0},                          // Rectangle définissant la zone de la texture à récupérer
          window_dimensions = {0},              // Rectangle définissant la fenêtre, on n'utilisera que largeur et hauteur
          destination = {0};                    // Rectangle définissant où la zone_source_image doit être déposée dans le renderer

  SDL_GetWindowSize(
      window, &window_dimensions.w,
      &window_dimensions.h);                    // Récupération des dimensions de la fenêtre
  SDL_QueryTexture(my_texture, NULL, NULL,
                   &source_image.w, &source_image.h);       // Récupération des dimensions de l'image
  
  destination.h = 20;
  destination.w = 20;
  
  
  play_with_texture_1_1(texture_back,window,
                        renderer);
  SDL_RenderPresent(renderer);                                  
  int i,j;
  for (i=0;i<taille;i++){
      for(j=0;j<taille;j++){
        if (matrice[i][j]==1){
          destination.x = i*20;          
          destination.y = j*20;
          SDL_RenderCopy(renderer, my_texture,
                &source_image,
                &destination);
        }
      }
  }
  SDL_RenderPresent(renderer);                                        // affichage

                                                                       // Pause exprimée en ms


}



/* ----------------------------------------------------------------------------------------------

    Trouver la classe d’un élément se fait en temps constant, mais fusionner deux classes prend
    un temps O(n), puisqu’il faut parcourir tout le tableau pour repérer les éléments dont il faut
    changer la classe. Une deuxième solution, que nous détaillons maintenant, consiste à choisir un
    représentant dans chaque classe. Fusionner deux classes revient alors à changer de représentant
    pour les éléments de la classe fusionnée. Il apparaı̂t avantageux de représenter la partition par
    une forêt. Chaque classe de la partition constitue un arbre de cette forêt. La racine de l’arbre
    est le représentant de sa classe.

 */

/* ----------------------------------------------------------------------------------------------
   DANS L IMPLEMENTATION ARBROESCENTE , ON UTILISE LA HAUTEUR QUI SERA PRINCIPALEMENT UTILISE DANS LA FUSION  */

partition_t initialisation(int taille)
{       
        partition_t partition;
        partition.taille=taille;
        partition.pere=malloc(taille*sizeof(int));
        partition.hauteur=malloc(taille*sizeof(int));
        for (int i = 0; i < partition.taille ; i++)
             {
                  partition.pere[i] = i;
                  partition.hauteur[i]=1;                   // LA HAUTEUR EST 1 INITIALEMENT
             }
        return partition ;    
}

/* ----------------------------------------------------------------------------------------------
   AFFICHAGE DES ELEMENTS DE L ENSEMBLE AVEC LEUR PERE ASSOCIE  */
void AfficherGrille(int** grille, int lig, int col)
{
    for (int i=0;i<lig;i++)
    {
        printf("[ ");
        for (int j=0;j<col;j++)
        {
            printf("%d ", grille[i][j]);
        }
        printf("]\n");
    }
}

void Affichage_partition(partition_t partition)
{
    printf("Les elements :");
    for (int i=0; i<partition.taille;i++)
        printf("%d ",i);
    printf("\n");    
    printf("Leur peres   :");    
    for (int i=0; i<partition.taille;i++)
        printf("%d ",partition.pere[i]);
    printf("\n -----------------------");
    printf("\n");    
}

/* ----------------------------------------------------------------------------------------------
   Chercher le représentant de la classe contenant un élément donné X revient à trouver la racine de
   l’arbre contenant un noeud donné. Ceci se fait par la méthode suivante :   */

int trouveracine(partition_t partition,int x)

{
    while (x != partition.pere[x])
        x = partition.pere[x];
    return x;
}

/* ----------------------------------------------------------------------------------------------
    Chacune des deux méthodes est de complexité O(h), où h
    est la hauteur l’arbre (la plus grande des hauteurs des deux arbres). En fait, on peut améliorer
    l’efficacité de l’algorithme par la règle suivante 
    
    
    Lors de l’union de deux arbres, la racine de l’arbre de moindre taille devient fils de la
    racine de l’arbre de plus grande taille.  */


void fusion(partition_t* partition,int x, int y)
{
    int r = trouveracine(*partition,x);                           // on cherche la racine de x et de y
    int s = trouveracine(*partition,y);
    if ((*partition).hauteur[r] >(*partition).hauteur[s] && r!=s)       
    {
        (*partition).pere[s] = r;
        (*partition).hauteur[s]=0;   
    }
    else if ((*partition).hauteur[r] < (*partition).hauteur[s]  && r!=s )
    {
        (*partition).pere[r] = s;
        (*partition).hauteur[r]=0;
    }
    else if ( (*partition).hauteur[r] == (*partition).hauteur[s]  && r!=s)
    {
        (*partition).pere[s] = r;
        (*partition).hauteur[s]=0;
        (*partition).hauteur[r]=(*partition).hauteur[r]+1;
    }
}



int * arreter_composantes(partition_t partition, int pere)
{
    int * arrete_composantes=malloc(sizeof(int));
    //int *tableau=malloc(partition.taille*sizeof(int));
    int k=1;
    for (int i=0;i<partition.taille;i++)
        {
            if (partition.pere[i]==pere)
               {  
                arrete_composantes[k]=i;
                k++; 
                arrete_composantes=(int*)realloc(arrete_composantes,k*sizeof(int));
               }      

        }
    arrete_composantes[0]=k;
    return arrete_composantes;
}


int existe(int * arrete,int taille,int element)
{
    int code=0;
    for (int i=0;i<taille;i++)
    {
        if (arrete[i]==element)
             code=1;
    }
    return code;
}

int nombre_racine(partition_t  partition)
{
    int nombrepere;
    int * arretepere=malloc(partition.taille*sizeof(int));
    for(int i=0;i<partition.taille;i++)
    {
        if  (!existe(arretepere,i,partition.pere[i]))
            nombrepere++;
        arretepere[i]=partition.pere[i];  
    }
    free(arretepere);
    return nombrepere;
}


graphe_t initialisationgraphe(int taille)
{
    graphe_t graphe;
    graphe.nombrenoeud=taille;
    graphe.nombrearrete=taille;
    graphe.arrete=malloc(taille*sizeof(arrete_t));

    // for (int k=0;k<graphe.nombrenoeud;k++)
    // {
    //     (graphe.arrete[k]).i=k;
    //     (graphe.arrete[k]).j=k;
    // }
    return graphe;
}

void affichergraphe(graphe_t graphe)
{
    printf("votre graphe est constitue par la arrete des arretes suivante : \n \n ");
    for (int k=0; k<graphe.nombrearrete;k++)
    {
        printf("(%d ,%d) |",graphe.arrete[k].i,graphe.arrete[k].j);
    }
    printf("\n ----------------- \n");
}

partition_t partitiondegraphe(graphe_t graphe)
{
    partition_t partition=initialisation(graphe.nombrenoeud);
    for (int k=0;k<(partition.taille);k++)
    {
        if (trouveracine(partition,graphe.arrete[k].i)!=trouveracine(partition,graphe.arrete[k].j) ) // s'il ne sont pas dans la meme classe
            fusion(&partition,graphe.arrete[k].i, graphe.arrete[k].j);
    }
    return partition;
}    


void arreter_partition(partition_t partition)
{
   printf(" \n Dans votre partition, les classes sont les suivant : ")  ;
   int i;
   for(i = 0; i < partition.taille; i++)
    {
        if(partition.pere[i] == i)                            // Les étiquettes de classes correspondent aux racines.
        printf("%d ", i);
    }
   printf("\n "); 
}

void arreter_classe(partition_t partition,int racine)     // Etiquette de classe.        
		                                 
{
    int classe;
    printf("la classe %d est l'ensemble :",racine);
    printf("{");
    for(int i = 0; i < partition.taille; i++)
    {
       classe= trouveracine(partition,partition.pere[i]);
       if(classe == racine)
         printf(" %d ", i);
    }
    printf("}"); 
}

void echanger(int *i,int *j)
{
    int tmp=*i;
    *i=*j;
    *j=tmp;
}

graphe_t Ficher_yate(graphe_t graphe)
{
    graphe_t ficher;
    int h;
    for (int l=graphe.nombrearrete-1;l>0;l--)
    {
        h=rand() % l;
        echanger(&graphe.arrete[l].i,&graphe.arrete[h].i);
        echanger(&graphe.arrete[l].j,&graphe.arrete[h].j);
    }
    ficher=graphe;
    return ficher;
}



graphe_t kruskal(graphe_t graphe)
 {
    partition_t partition = initialisation(graphe.nombrenoeud); // 1- creation d une partition dont les elements sont les noeuds du graphe
    graphe_t A=initialisationgraphe(graphe.nombrenoeud); // creation d'une arrete d'arretes vide
    A.nombrearrete=0;
    for (int k=0;k<graphe.nombrearrete;k++)
    {
        if (trouveracine(partition,graphe.arrete[k].i)!=trouveracine(partition,graphe.arrete[k].j))
        {
            fusion(&partition,graphe.arrete[k].i,graphe.arrete[k].j);
            A.nombrearrete++;
            A.arrete[A.nombrearrete-1].i=graphe.arrete[k].i;
            A.arrete[A.nombrearrete-1].j=graphe.arrete[k].j;
        }
    }
    return A;
 }



graphe_t generation_aleatoire (int nombrenoeud)
{
    graphe_t graphe_aleatoire=initialisationgraphe(nombrenoeud);
    int n=rand()% 2*nombrenoeud  ; // le nombre d'arrete est aleatoire aussi
    graphe_aleatoire.nombrearrete=n;
    int k;
    for (int l=0;l<n;l++)
    {
        k=rand() % nombrenoeud ;
        graphe_aleatoire.arrete[l].i=k;
        k=rand() % nombrenoeud;
        graphe_aleatoire.arrete[l].j=k;
    } 
    return graphe_aleatoire;
}

// aij=a11+p(i-1)+(j-1)
graphe_t generation_graphe(int n)
{
    graphe_t graphe=initialisationgraphe(n*n);
    graphe.nombrearrete=2*n*n;
    for (int k=0;k<n;k++)
    {
        for (int h=0;h<n;h++)
        {
            graphe.arrete[n*k+h].i= n*k+h;
            graphe.arrete[n*k+h].j= n*k+h+1;
        }
    }
    return graphe;
}
//taille+1 = taille matrice 
graphe_t generation_grille(int taille){
    taille--;
    graphe_t graphe=initialisationgraphe(4*taille +2*(taille-1)*taille);  
    int a,b;
    int cpt =0;
    for(int k =0;k<(taille+1)*(taille+1);k++){
        a = k+taille+1;
        b = k+1;
        if((k+1)%(taille+1)==0 && a<(taille+1)*(taille+1)){
            // printf("{%d - %d} -> |\n",k,a); // les case a droites
            graphe.arrete[cpt].i=k;
            graphe.arrete[cpt].j=a;
            cpt++;
        }
        if(taille>=(taille+1)*(taille+1)-k-1 && b<(taille+1)*(taille+1)){
            // printf("{%d - %d} -> --\n",k,b); //// les case en bas
            graphe.arrete[cpt].i=k;
            graphe.arrete[cpt].j=b;
            cpt++;
        }

        else if ( (k+1)%(taille+1)!=0 ){
            graphe.arrete[cpt].i=k;
            graphe.arrete[cpt].j=a;
            cpt++;
            graphe.arrete[cpt].i=k;
            graphe.arrete[cpt].j=b;
            cpt++;
            // printf("{%d - %d} -> |\n",k,a);
            // printf("{%d - %d} -> --\n",k,b);
        }          
    }
    return graphe;
}
int ** generer_matrice(int taille){
    int ** matrice=malloc(taille*sizeof(int*));
    for(int k=0;k<taille;k++){
        matrice[k]=malloc(taille*sizeof(int));
        for(int l=0;l<taille;l++){
            matrice[k][l]=N+S+O+E;
    } 
    }
    return matrice;
}
int** graph_en_matrice(int taille){
    graphe_t graphe= generation_grille(taille);
    graphe = Ficher_yate(graphe);
    graphe = Ficher_yate(graphe);
    graphe = Ficher_yate(graphe);
    graphe = kruskal(graphe);
    tracer_graphe(graphe, "labyrinthe.dot");

    int ligne,colonne;
    int **matrice =generer_matrice(taille);
    for(int k =0;k<=graphe.nombrearrete;k++){
       colonne = (graphe.arrete[k].i)%taille;
       ligne = (graphe.arrete[k].i)/taille;
       
       if(colonne<taille && (graphe.arrete[k].i)+1==(graphe.arrete[k].j)){
           matrice[ligne][colonne]-=E;
           matrice[ligne][colonne+1]-=O;
       }
       if(ligne<taille && (graphe.arrete[k].i)+taille==(graphe.arrete[k].j) ){
           matrice[ligne][colonne]-=S;
           matrice[ligne+1][colonne]-=N;
       }
    }
    return matrice;
}
int** cree(int ligne,int colonne){
    int ** matrice = NULL;
    matrice = malloc(ligne*sizeof(int*));
    int i = 0; 
    for(i=0;i<ligne;i++){
        matrice[i]=malloc(colonne*sizeof(int));
    }
    return matrice;
}
void fill_matrix(int** matrice,int ligne,int colonne){
    int i,j;
    for (i=0;i<ligne;i++){
        for(j=0;j<colonne;j++){
            matrice[i][j]=0;
            if(i==0 || j==0 || i ==(ligne-1) || j==(colonne-1)){
                matrice[i][j]=1;
            }
            
        }
    }
}
//matrice 1 si mure 0 sinon
int **matrice_a_dessiner(int** matrice,int taille){
    int t = taille*2+1;
    int ** final =cree(t,t);
    int k ,l;
    fill_matrix(final,t,t);
    for(int i =0;i<taille;i++){
        for(int j=0;j<taille;j++){
            k = 2*i +1;
            l = 2*j +1;
            if(matrice[i][j] & E){
                final[k][l+1]=1;

            }
            if(matrice[i][j] & S){
                final[k+1][l]=1;
            }
            final[k+1][l+1]=1;
        }
    }
    return final;
}

int main ()
{
    int taille = 20;
    int** matrice = graph_en_matrice(taille);
    int** final=matrice_a_dessiner(matrice,taille);
    int t = taille*2+1;
    if(SDL_Init(SDL_INIT_VIDEO) < 0)     
    {         
        SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "[DEBUG] > %s", SDL_GetError());         
        return EXIT_FAILURE;     
    }    
    SDL_Texture  * texture1 = NULL; 
    SDL_Texture  * texture2 = NULL; 
    
    SDL_Window   * pWindow;  
    SDL_Renderer * pRenderer; 
    pWindow = SDL_CreateWindow("Labyrinthe",SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, t*20, t*20, SDL_WINDOW_SHOWN);
    if (pWindow == NULL)     
    {         
        SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "[DEBUG] > %s", SDL_GetError());         
        SDL_Quit();         
        return EXIT_FAILURE;     
    }  
    pRenderer = SDL_CreateRenderer(pWindow, -1, SDL_RENDERER_ACCELERATED);       
    if (pRenderer == NULL)     
    {         
        SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "[DEBUG] > %s", SDL_GetError());         
        SDL_Quit();         
        return EXIT_FAILURE;     
    }  
    texture2=load_texture_from_image("mur.png",pWindow,pRenderer);
    texture1=load_texture_from_image("Back.jpg",pWindow, pRenderer);   /*     creation de la  texture qui contient l'image    */
    SDL_bool program_on = SDL_TRUE;               // Booléen pour dire que le programme doit continuer
    while (program_on){                           // Voilà la boucle des évènements
        SDL_Event event;                            // c'est le type IMPORTANT !!
        play_with_texture_2(texture2,texture1, pWindow,
                                pRenderer,final,t);   
        while(program_on && SDL_WaitEvent(&event)){ // Tant que la file des évènements stockés n'est
                                                    // pas vide et qu'on n'a pas terminé le programme :
                                     //    Défiler l'élément en tête de file dans 'event'
            switch(event.type){                       // En fonction de la valeur du type de cet évènement
                case SDL_QUIT :                           // Un évènement simple, on a cliqué sur la x de la fenêtre
                program_on = SDL_FALSE;                 // Il est temps d'arrêter le programme
                break;

            default:
                                  // L'évènement défilé ne nous intéresse pas
                break;
                }
        }
                                          // Affichages et calculs souvent ici
    }  
    

    SDL_DestroyRenderer(pRenderer);      
    SDL_DestroyWindow(pWindow);
    SDL_Quit();
    
    
    return EXIT_SUCCESS;    
 }


