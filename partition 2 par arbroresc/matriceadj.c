#include "matriceadj.h"

// genere une matrice adjacente

int ** matriceadjac(int taille)
{
    int** matrice = NULL;
    matrice = malloc(taille*sizeof(int*));
    srand(time(NULL));
    for (int i=0;i<taille;i++)
    {
        matrice[i] = malloc(taille*sizeof(int));
    }
    matrice[0][0] = 1;
    for (int i=1;i<taille;i++)
    {
        matrice[i][i] = 1;
        for (int j=0;j<i;j++)
        {
            
            matrice[i][j] = rand() % 2;
            matrice[j][i] = matrice[i][j];

        }
    }
    return matrice;

}

// affiche une matrice

void Affichermatrice(int** matrice, int taille)
{
    for (int i=0;i<taille;i++)
    {
        printf("[ ");
        for (int j=0;j<taille;j++)
        {
            printf("%d ", matrice[i][j]);
        }
        printf("]\n");
    }
}

// transforme la matrice en une partition

partition_t MatEnPart(int** mat, int taille)
{
    partition_t p = initialisation(taille);
    for (int i=1;i<taille;i++)
    {
        for (int j=0;j<i;j++)
        {
            if (mat[i][j]) fusion(p, i, j);
        }
    }
    return p;
}

//int main(void)
//{
//    int ** m = matriceadjac(6);
//    Affichermatrice(m,6);
//    partition_t p = PartitionEnMat(m, 6);
//
//    Affichage_partition(p);
//
//    return 0;
//}
