#ifndef PARTITION__H
#define PARTITION__H

#include <stdlib.h>
#include <stdio.h>


typedef struct partition
{

    int   taille;
    int * hauteur;
    int * pere;
}partition_t;


partition_t initialisation(int taille);
void Affichage_partition(partition_t partition);
int trouver(partition_t partition,int x);
partition_t fusion(partition_t partition,int x, int y);
int * lister_composantes(partition_t partition, int pere);
int existe(int * liste,int taille,int element);
int nombre_connexe(partition_t  partition);
void affichage_composante(int * liste);

#endif // PARTITION__H
