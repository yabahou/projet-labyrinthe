#include "partition.h"

partition_t* creer(int taille){
    partition_t* p = malloc(sizeof(partition_t));
    p->taille=taille;
    p->valeur = malloc(taille*sizeof(int));
    p->classe = malloc(taille*sizeof(int));
    return p;
}
int recuperer_classe(partition_t* p, int val){
    int i = 0;
    while (p->valeur[i]!=val)
    {
        i++;
    }
    return p->classe[i];
}
void fusion(partition_t*p,int x,int y){
    int i = 0,j=0;
    while (p->valeur[i]!= MIN(x,y))
    {
        i++;
    }
    while (p->valeur[j]!= MAX(x,y))
    {
        j++;
    }
    p->classe[j]=p->classe[i];
}
// int taille(int* array){
//     int t = (int) sizeof(array)/sizeof(int);
//     return t;
// }
int * lister_classe1(partition_t*p,int etiquette,int* taille_classe){
    int * tableau = malloc(sizeof(int));
    *taille_classe=1;
    int code = 1;
    for (int i=0;i<p->taille;i++){
        if(etiquette==p->classe[i]){
            code=0;
            tableau[(*taille_classe)-1]= p->valeur[i];
            tableau = (int*)realloc(tableau,(*taille_classe)*sizeof(int)); 
            (*taille_classe)++;        
        }
    }
    (*taille_classe)--;
    if(code){
        free(tableau);
    }
    return tableau;
}

int existe(int*array,int valeur,int taille){
    int resultat =0;
    int i=0;
    while((i<taille) && (resultat==0)){
        if(array[i]==valeur) resultat = 1;
        i++;
    }
    return resultat;
}
int ** lister_partition(partition_t*p){
    int**resultat = malloc(sizeof(int*));
    int* classe_inseree = malloc(sizeof(int));    
    int taille=0;
    int k =0;
    resultat = (int**) realloc(resultat,(taille+1)*sizeof(int*));
    resultat[taille]=lister_classe(p,p->classe[0],&k);
    classe_inseree = (int*) realloc(classe_inseree,(taille+1)*sizeof(int)); 
    classe_inseree[taille+1]=p->classe[0];
    taille++;
    for (int i=1;i<p->taille;i++){
        if(!(existe(classe_inseree,(p->classe[i]),taille))){
            resultat = (int**) realloc(resultat,(taille+1)*sizeof(int*));
            resultat[taille]=lister_classe(p,p->classe[i],&k);           
            classe_inseree = (int*) realloc(classe_inseree,(taille+1)*sizeof(int)); 
            classe_inseree[taille]=p->classe[i];
            taille++;
        }
               
    }
    return resultat;
}

int main(){
    partition_t* p = creer(6);

    for (int i=0;i<6;i++){
        p->valeur[i] = i*i;
    }
    for (int i=0;i<6;i++){
        p->classe[i] = i;
    }
    fusion(p,16,25);
    int** par = lister_partition(p);
    for(int i=0;i<5;i++){
        for(int j =0;j<2;j++){
            printf("%d ",par[i][j]);
        }
        printf("\n");
    }
    
    
    return 0;
}