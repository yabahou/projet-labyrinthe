#ifndef PARTITION__H
#define PARTITION__H

#define MIN(X, Y) (((X) < (Y)) ? (X) : (Y))
#define MAX(X, Y) (((X) > (Y)) ? (X) : (Y))

#include <stdio.h>
#include <stdlib.h>
#include <math.h>



typedef struct partition
{
    int *valeur;
    int *classe;
    int taille;
}partition_t;

#endif