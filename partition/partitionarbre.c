#include <stdlib.h>
#include <stdio.h>
#include "partitionarbre.h"



foret_t initialisation(int taille)
{       
        foret_t foret;
        foret.taille=taille;
        foret.pere=malloc(taille*sizeof(int));
        foret.hauteur=malloc(taille*sizeof(int));
        for (int i = 0; i < foret.taille ; i++)
             {
                  foret.pere[i] = i;
                  foret.hauteur[i]=1;
             }
        return foret ;    
}

void Affichage_foret(foret_t foret)
{
    printf("Les elements :");
    for (int i=0; i<foret.taille;i++)
        printf("%d ",i);
    printf("\n");    
    printf("Leur peres   :");    
    for (int i=0; i<foret.taille;i++)
        printf("%d ",foret.pere[i]);
    printf("\nLa hauteur   :");    
    for (int i=0; i<foret.taille;i++)
        printf("%d ",foret.hauteur[i]);
    printf("\n -----------------------");
    printf("\n");    
}

int trouver(foret_t foret,int x)

{
    while (x != foret.pere[x])
        x = foret.pere[x];
    return x;
}

int taille (foret_t foret,int classe)
 {
    int k=0;
    for (int i=0;i<foret.taille;i++)
    {
        if (foret.pere[i]==classe)
            k++;
    }
    return k;
 }

foret_t unionPonderee(foret_t foret,int x, int y)
{
    int r = trouver(foret,x);
    int s = trouver(foret,y);
    if (foret.hauteur[r] >= foret.hauteur[s])
    {
        foret.pere[s] = r;
        foret.hauteur[r]+=foret.hauteur[s];
    }
    else
    {
        foret.pere[r] = s;
        foret.hauteur[s]+=foret.hauteur[r];
    }
    return foret;
}



int main ()
{
    foret_t foret;
    foret=initialisation(11);
    Affichage_foret(foret);
    unionPonderee(foret,0,1);
    Affichage_foret(foret);
    unionPonderee(foret,2,3);
    Affichage_foret(foret);
    unionPonderee(foret,10,3);
    Affichage_foret(foret);
    unionPonderee(foret,5,9);
    Affichage_foret(foret);
    unionPonderee(foret,4,6);
    Affichage_foret(foret);
    unionPonderee(foret,8,7);
    Affichage_foret(foret);
    unionPonderee(foret,9,7);
    Affichage_foret(foret);
    unionPonderee(foret,6,8);
    Affichage_foret(foret);


}


